# 1. Variable
Variable adalah suatu tempat penyimpanan sementara yang dapat menampung suatu nilai atau data.
Contoh variable yang sering kita lihat adalah Skor pada game Dino Chrome. Bisa dilihat pada gambar dibawah ini di bagian atas paling kanan itu adalah Variable.

![Dino Game](imgs/dinogame.gif)

Variable dalam bahasa PHP harus diawali dengan tanda $ (dolar) dan disusul dengan huruf atau underscore dan tidak diawali dengan angka (setelah tanda $ dolar).
Contoh Penulisan Variable dalam PHP:
```php
<?php
$score = "51";
$_score = "51"; 

echo $score; // output 51
echo $_score; // output 51
?>
```

Tambahan **echo** adalah fungsi untuk menampilkan suatu variable didalam PHP.

Peraturan dalam menulis variable di PHP:

    - Diawali dengan tanda $ (dolar) dan disusul dengan huruf atau underscore
    - Tidak boleh disusul dengan angka (setelah tanda $ dolar)
    - Boleh menggunakan angka jika penulisannya seperti ini $score1, $_score1.
    - Nama variable hanya dapat di isi oleh (a-z,A-Z,0-9,_)
    - Nama variable mengandung Case Sensitive yang berarti besar kecil berpengaruh